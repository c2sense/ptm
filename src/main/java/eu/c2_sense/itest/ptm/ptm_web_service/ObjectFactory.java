//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.7 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2017.03.15 alle 11:24:56 AM CET 
//


package eu.c2_sense.itest.ptm.ptm_web_service;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.c2_sense.itest.ptm.ptm_web_service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.c2_sense.itest.ptm.ptm_web_service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTopicResponse }
     * 
     */
    public GetTopicResponse createGetTopicResponse() {
        return new GetTopicResponse();
    }

    /**
     * Create an instance of {@link Topic }
     * 
     */
    public Topic createTopic() {
        return new Topic();
    }

    /**
     * Create an instance of {@link GetTransactionResponse }
     * 
     */
    public GetTransactionResponse createGetTransactionResponse() {
        return new GetTransactionResponse();
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link GetProfileResponse }
     * 
     */
    public GetProfileResponse createGetProfileResponse() {
        return new GetProfileResponse();
    }

    /**
     * Create an instance of {@link Profile }
     * 
     */
    public Profile createProfile() {
        return new Profile();
    }

    /**
     * Create an instance of {@link GetTopicsByProfileIdAndActorIdRequest }
     * 
     */
    public GetTopicsByProfileIdAndActorIdRequest createGetTopicsByProfileIdAndActorIdRequest() {
        return new GetTopicsByProfileIdAndActorIdRequest();
    }

    /**
     * Create an instance of {@link GetActorRequest }
     * 
     */
    public GetActorRequest createGetActorRequest() {
        return new GetActorRequest();
    }

    /**
     * Create an instance of {@link GetProfileListResponse }
     * 
     */
    public GetProfileListResponse createGetProfileListResponse() {
        return new GetProfileListResponse();
    }

    /**
     * Create an instance of {@link GetActorListRequest }
     * 
     */
    public GetActorListRequest createGetActorListRequest() {
        return new GetActorListRequest();
    }

    /**
     * Create an instance of {@link GetTopicNameByProfileIdAndTransactionIdRequest }
     * 
     */
    public GetTopicNameByProfileIdAndTransactionIdRequest createGetTopicNameByProfileIdAndTransactionIdRequest() {
        return new GetTopicNameByProfileIdAndTransactionIdRequest();
    }

    /**
     * Create an instance of {@link GetAllTopicMapRequest }
     * 
     */
    public GetAllTopicMapRequest createGetAllTopicMapRequest() {
        return new GetAllTopicMapRequest();
    }

    /**
     * Create an instance of {@link GetProfileRequest }
     * 
     */
    public GetProfileRequest createGetProfileRequest() {
        return new GetProfileRequest();
    }

    /**
     * Create an instance of {@link GetTopicListRequest }
     * 
     */
    public GetTopicListRequest createGetTopicListRequest() {
        return new GetTopicListRequest();
    }

    /**
     * Create an instance of {@link GetTransactionRequest }
     * 
     */
    public GetTransactionRequest createGetTransactionRequest() {
        return new GetTransactionRequest();
    }

    /**
     * Create an instance of {@link GetTopicListResponse }
     * 
     */
    public GetTopicListResponse createGetTopicListResponse() {
        return new GetTopicListResponse();
    }

    /**
     * Create an instance of {@link GetAllTopicMapResponse }
     * 
     */
    public GetAllTopicMapResponse createGetAllTopicMapResponse() {
        return new GetAllTopicMapResponse();
    }

    /**
     * Create an instance of {@link TopicMap }
     * 
     */
    public TopicMap createTopicMap() {
        return new TopicMap();
    }

    /**
     * Create an instance of {@link GetTopicsByProfileIdAndActorIdResponse }
     * 
     */
    public GetTopicsByProfileIdAndActorIdResponse createGetTopicsByProfileIdAndActorIdResponse() {
        return new GetTopicsByProfileIdAndActorIdResponse();
    }

    /**
     * Create an instance of {@link GetTopicNameByProfileIdAndTransactionIdResponse }
     * 
     */
    public GetTopicNameByProfileIdAndTransactionIdResponse createGetTopicNameByProfileIdAndTransactionIdResponse() {
        return new GetTopicNameByProfileIdAndTransactionIdResponse();
    }

    /**
     * Create an instance of {@link GetTransactionListResponse }
     * 
     */
    public GetTransactionListResponse createGetTransactionListResponse() {
        return new GetTransactionListResponse();
    }

    /**
     * Create an instance of {@link GetProfileListRequest }
     * 
     */
    public GetProfileListRequest createGetProfileListRequest() {
        return new GetProfileListRequest();
    }

    /**
     * Create an instance of {@link GetActorListResponse }
     * 
     */
    public GetActorListResponse createGetActorListResponse() {
        return new GetActorListResponse();
    }

    /**
     * Create an instance of {@link Actor }
     * 
     */
    public Actor createActor() {
        return new Actor();
    }

    /**
     * Create an instance of {@link GetActorResponse }
     * 
     */
    public GetActorResponse createGetActorResponse() {
        return new GetActorResponse();
    }

    /**
     * Create an instance of {@link GetTopicRequest }
     * 
     */
    public GetTopicRequest createGetTopicRequest() {
        return new GetTopicRequest();
    }

    /**
     * Create an instance of {@link GetTransactionListRequest }
     * 
     */
    public GetTransactionListRequest createGetTransactionListRequest() {
        return new GetTransactionListRequest();
    }

}

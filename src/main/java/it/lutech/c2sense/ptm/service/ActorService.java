
package it.lutech.c2sense.ptm.service;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import eu.c2_sense.itest.ptm.ptm_web_service.Actor;





@Service
@Transactional
public class ActorService
{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Actor findActor( String id )
	{
		Assert.notNull( id );

		String sql = "select * from actor where id = ?";

		return jdbcTemplate.query( sql, new String [ ] { id }, new ActorResultSetExtractor( ) );

	}

	public List < Actor > listActor( )
	{
		String sql = "select * from actor";
		return jdbcTemplate.query( sql, new ActorRowMapper( ) );
	}

	public static class ActorResultSetExtractor implements ResultSetExtractor < Actor >
	{

		@Override
		public Actor extractData( ResultSet rs ) throws SQLException, DataAccessException
		{
			Actor actor = null;
			while ( rs.next( ) )
			{
				actor = new Actor( );
				actor.setId( rs.getString( "id" ) );
				actor.setName( rs.getString( "name" ) );
				actor.setDescription( rs.getString( "description" ) );
			}
			return actor;
		}
	}

	public static class ActorRowMapper implements RowMapper < Actor >
	{
		@Override
		public Actor mapRow( ResultSet rs, int rowNum ) throws SQLException
		{
			Actor actor = new Actor( );

			actor.setId( rs.getString( "id" ) );
			actor.setName( rs.getString( "name" ) );
			actor.setDescription( rs.getString( "description" ) );

			return actor;
		}
	}
}


package it.lutech.c2sense.ptm.service;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import eu.c2_sense.itest.ptm.ptm_web_service.Transaction;





@Service
@Transactional
public class TransactionService
{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Transaction findTransaction( String id )
	{
		Assert.notNull( id );

		String sql = "select * from transaction where id = ?";

		return jdbcTemplate.query( sql, new String [ ] { id }, new TransactionResultSetExtractor( ) );

	}

	public List < Transaction > listTransaction( )
	{
		String sql = "select * from transaction";
		return jdbcTemplate.query( sql, new TransactionRowMapper( ) );
	}

	public static class TransactionResultSetExtractor implements ResultSetExtractor < Transaction >
	{

		@Override
		public Transaction extractData( ResultSet rs ) throws SQLException, DataAccessException
		{
			Transaction transaction = null;
			while ( rs.next( ) )
			{
				transaction = new Transaction( );
				transaction.setId( rs.getString( "id" ) );
				transaction.setName( rs.getString( "name" ) );
				transaction.setDescription( rs.getString( "description" ) );
			}
			return transaction;
		}
	}

	public static class TransactionRowMapper implements RowMapper < Transaction >
	{
		@Override
		public Transaction mapRow( ResultSet rs, int rowNum ) throws SQLException
		{
			Transaction transaction = new Transaction( );

			transaction.setId( rs.getString( "id" ) );
			transaction.setName( rs.getString( "name" ) );
			transaction.setDescription( rs.getString( "description" ) );

			return transaction;
		}
	}
}


package it.lutech.c2sense.ptm.service;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import eu.c2_sense.itest.ptm.ptm_web_service.Profile;





@Service
@Transactional
public class ProfileService
{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Profile findProfile( String id )
	{
		Assert.notNull( id );

		String sql = "select * from profile where id = ?";

		return jdbcTemplate.query( sql, new String [ ] { id }, new ProfileResultSetExtractor( ) );

	}

	public List < Profile > listProfile( )
	{
		String sql = "select * from profile";
		return jdbcTemplate.query( sql, new ProfileRowMapper( ) );
	}

	public static class ProfileResultSetExtractor implements ResultSetExtractor < Profile >
	{

		@Override
		public Profile extractData( ResultSet rs ) throws SQLException, DataAccessException
		{
			Profile profile = null;
			while ( rs.next( ) )
			{
				profile = new Profile( );
				profile.setId( rs.getString( "id" ) );
				profile.setName( rs.getString( "name" ) );
				profile.setDescription( rs.getString( "description" ) );
			}
			return profile;
		}
	}

	public static class ProfileRowMapper implements RowMapper < Profile >
	{
		@Override
		public Profile mapRow( ResultSet rs, int rowNum ) throws SQLException
		{
			Profile profile = new Profile( );

			profile.setId( rs.getString( "id" ) );
			profile.setName( rs.getString( "name" ) );
			profile.setDescription( rs.getString( "description" ) );

			return profile;
		}
	}
}

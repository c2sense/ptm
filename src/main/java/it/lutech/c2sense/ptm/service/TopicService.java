
package it.lutech.c2sense.ptm.service;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import eu.c2_sense.itest.ptm.ptm_web_service.Topic;





@Service
@Transactional
public class TopicService
{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Topic findTopic( String id )
	{
		Assert.notNull( id );

		String sql = "select * from topic where id = ?";

		return jdbcTemplate.query( sql, new String [ ] { id }, new TopicResultSetExtractor( ) );

	}

	public List < Topic > listTopic( )
	{
		String sql = "select * from topic";
		return jdbcTemplate.query( sql, new TopicRowMapper( ) );
	}

	public static class TopicResultSetExtractor implements ResultSetExtractor < Topic >
	{

		@Override
		public Topic extractData( ResultSet rs ) throws SQLException, DataAccessException
		{
			Topic topic = null;
			while ( rs.next( ) )
			{
				topic = new Topic( );
				topic.setId( rs.getString( "id" ) );
				topic.setName( rs.getString( "name" ) );
				topic.setDescription( rs.getString( "description" ) );
			}
			return topic;
		}
	}

	public static class TopicRowMapper implements RowMapper < Topic >
	{
		@Override
		public Topic mapRow( ResultSet rs, int rowNum ) throws SQLException
		{
			Topic topic = new Topic( );

			topic.setId( rs.getString( "id" ) );
			topic.setName( rs.getString( "name" ) );
			topic.setDescription( rs.getString( "description" ) );

			return topic;
		}
	}
}


package it.lutech.c2sense.ptm.service;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import eu.c2_sense.itest.ptm.ptm_web_service.TopicMap;





@Service
@Transactional
public class TopicMapService
{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List < TopicMap > findAllTopicMap( )
	{
		String sql = "select tm.profile_id, t.name as transaction_id, tm.actor_id, tm.read_topic_id, tm.write_topic_id from topic_map tm join transaction t on tm.transaction_id = t.id";

		return jdbcTemplate.query( sql, new TopicMapRowMapper( ) );
	}

	public List < TopicMap > findTopicMapByProfileIdAndActorId( String profileId, String actorId )
	{
		Assert.notNull( profileId );
		Assert.notNull( actorId );

		String sql = "select tm.profile_id, t.name as transaction_id, tm.actor_id, tm.read_topic_id, tm.write_topic_id from topic_map tm join transaction t on tm.transaction_id = t.id where profile_id = ? and actor_id = ?";

		return jdbcTemplate.query( sql, new String [ ] { profileId, actorId }, new TopicMapRowMapper( ) );
	}

	public List < TopicMap > findTopicMapByProfileIdAndTransactionId( String profileId, String transactionId )
	{
		Assert.notNull( profileId );
		Assert.notNull( transactionId );

		String sql = "select tm.profile_id, t.name as transaction_id, tm.actor_id, tm.read_topic_id, tm.write_topic_id from topic_map tm join transaction t on tm.transaction_id = t.id where profile_id = ? and transaction_id = ?";

		return jdbcTemplate.query( sql, new String [ ] { profileId, transactionId }, new TopicMapRowMapper( ) );
	}

	public static class TopicMapRowMapper implements RowMapper < TopicMap >
	{
		@Override
		public TopicMap mapRow( ResultSet rs, int rowNum ) throws SQLException
		{
			TopicMap topicMap = new TopicMap( );
			topicMap.setProfileId( rs.getString( "profile_id" ) );
			topicMap.setTransactionId( rs.getString( "transaction_id" ) );
			topicMap.setActorId( rs.getString( "actor_id" ) );

			topicMap.setWrite( rs.getString( "write_topic_id" ) );
			topicMap.setRead( rs.getString( "read_topic_id" ) );
			return topicMap;
		}
	}
}


package it.lutech.c2sense.ptm.endpoint;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import eu.c2_sense.itest.ptm.ptm_web_service.GetActorListRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetActorListResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetActorRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetActorResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetAllTopicMapRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetAllTopicMapResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetProfileListRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetProfileListResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetProfileRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetProfileResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicListRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicListResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicNameByProfileIdAndTransactionIdRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicNameByProfileIdAndTransactionIdResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicsByProfileIdAndActorIdRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicsByProfileIdAndActorIdResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTransactionListRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTransactionListResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTransactionRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTransactionResponse;
import it.lutech.c2sense.ptm.service.ActorService;
import it.lutech.c2sense.ptm.service.ProfileService;
import it.lutech.c2sense.ptm.service.TopicMapService;
import it.lutech.c2sense.ptm.service.TopicService;
import it.lutech.c2sense.ptm.service.TransactionService;





@Endpoint
public class PtmEndpoint
{
	private static final String	NAMESPACE_URI	= "http://ptm.itest.c2-sense.eu/ptm-web-service";

	private ActorService		actorService;
	private TopicService		topicService;
	private TopicMapService		topicMapService;
	private TransactionService	transactionService;
	private ProfileService		profileService;

	@Autowired
	public PtmEndpoint( ActorService actorService,
		TopicService topicService,
		TransactionService transactionService,
		ProfileService profileService,
		TopicMapService topicMapService )
	{
		this.actorService = actorService;
		this.topicService = topicService;
		this.topicMapService = topicMapService;
		this.transactionService = transactionService;
		this.profileService = profileService;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getActorRequest" )
	@ResponsePayload
	public GetActorResponse getActor( @RequestPayload GetActorRequest request )
	{
		GetActorResponse response = new GetActorResponse( );
		response.setActor( actorService.findActor( request.getId( ) ) );

		return response;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getActorListRequest" )
	@ResponsePayload
	public GetActorListResponse listActor( @RequestPayload GetActorListRequest request )
	{
		GetActorListResponse response = new GetActorListResponse( );
		response.getActor( ).addAll( actorService.listActor( ) );

		return response;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getProfileRequest" )
	@ResponsePayload
	public GetProfileResponse getProfile( @RequestPayload GetProfileRequest request )
	{
		GetProfileResponse response = new GetProfileResponse( );
		response.setProfile( profileService.findProfile( request.getId( ) ) );

		return response;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getProfileListRequest" )
	@ResponsePayload
	public GetProfileListResponse listProfile( @RequestPayload GetProfileListRequest request )
	{
		GetProfileListResponse response = new GetProfileListResponse( );
		response.getProfile( ).addAll( profileService.listProfile( ) );

		return response;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getTransactionRequest" )
	@ResponsePayload
	public GetTransactionResponse getTransaction( @RequestPayload GetTransactionRequest request )
	{
		GetTransactionResponse response = new GetTransactionResponse( );
		response.setTransaction( transactionService.findTransaction( request.getId( ) ) );

		return response;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getTransactionListRequest" )
	@ResponsePayload
	public GetTransactionListResponse listTransaction( @RequestPayload GetTransactionListRequest request )
	{
		GetTransactionListResponse response = new GetTransactionListResponse( );
		response.getTransaction( ).addAll( transactionService.listTransaction( ) );

		return response;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getTopicRequest" )
	@ResponsePayload
	public GetTopicResponse getTopic( @RequestPayload GetTopicRequest request )
	{
		GetTopicResponse response = new GetTopicResponse( );
		response.setTopic( topicService.findTopic( request.getId( ) ) );

		return response;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getTopicListRequest" )
	@ResponsePayload
	public GetTopicListResponse listTopic( @RequestPayload GetTopicListRequest request )
	{
		GetTopicListResponse response = new GetTopicListResponse( );
		response.getTopic( ).addAll( topicService.listTopic( ) );

		return response;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getAllTopicMapRequest" )
	@ResponsePayload
	public GetAllTopicMapResponse getAllTopicMaps( @RequestPayload GetAllTopicMapRequest request )
	{
		GetAllTopicMapResponse response = new GetAllTopicMapResponse( );
		response.getTopicMap( ).addAll( topicMapService.findAllTopicMap( ) );

		return response;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getTopicsByProfileIdAndActorIdRequest" )
	@ResponsePayload
	public GetTopicsByProfileIdAndActorIdResponse getTopicsByProfileIdAndActorIdRequest( @RequestPayload GetTopicsByProfileIdAndActorIdRequest request )
	{
		GetTopicsByProfileIdAndActorIdResponse response = new GetTopicsByProfileIdAndActorIdResponse( );
		response.getTopicMap( ).addAll( topicMapService.findTopicMapByProfileIdAndActorId( request.getProfileId( ), request.getActorId( ) ) );

		return response;
	}

	@PayloadRoot( namespace = NAMESPACE_URI, localPart = "getTopicNameByProfileIdAndTransactionIdRequest" )
	@ResponsePayload
	public GetTopicNameByProfileIdAndTransactionIdResponse getTopicNameByProfileIdAndTransactionIdRequest( @RequestPayload GetTopicNameByProfileIdAndTransactionIdRequest request )
	{
		GetTopicNameByProfileIdAndTransactionIdResponse response = new GetTopicNameByProfileIdAndTransactionIdResponse( );
		response.getTopicMap( ).addAll( topicMapService.findTopicMapByProfileIdAndTransactionId( request.getProfileId( ), request.getTransactionId( ) ) );

		return response;
	}

}

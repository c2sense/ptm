
package it.lutech.c2sense.ptm.model;



public class TopicMap extends eu.c2_sense.itest.ptm.ptm_web_service.TopicMap
{

	public TopicMap( String profileId, String transactionId, String actorId, String read, String write )
	{
		super( );
		this.profileId = profileId;
		this.transactionId = transactionId;
		this.actorId = actorId;
		this.read = read;
		this.write = write;
	}

	@Override
	public boolean equals( Object obj )
	{
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( eu.c2_sense.itest.ptm.ptm_web_service.TopicMap.class != obj.getClass( ) )
			return false;
		eu.c2_sense.itest.ptm.ptm_web_service.TopicMap other = ( eu.c2_sense.itest.ptm.ptm_web_service.TopicMap ) obj;
		if ( actorId == null )
		{
			if ( other.getActorId( ) != null )
				return false;
		}
		else if ( ! actorId.equals( other.getActorId( ) ) )
			return false;
		if ( profileId == null )
		{
			if ( other.getProfileId( ) != null )
				return false;
		}
		else if ( ! profileId.equals( other.getProfileId( ) ) )
			return false;
		if ( read == null )
		{
			if ( other.getRead( ) != null )
				return false;
		}
		else if ( ! read.equals( other.getRead( ) ) )
			return false;
		if ( transactionId == null )
		{
			if ( other.getTransactionId( ) != null )
				return false;
		}
		else if ( ! transactionId.equals( other.getTransactionId( ) ) )
			return false;
		if ( write == null )
		{
			if ( other.getWrite( ) != null )
				return false;
		}
		else if ( ! write.equals( other.getWrite( ) ) )
			return false;
		return true;
	}

}

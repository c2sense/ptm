
package it.lutech.c2sense.ptm;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicNameByProfileIdAndTransactionIdRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicNameByProfileIdAndTransactionIdResponse;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicsByProfileIdAndActorIdRequest;
import eu.c2_sense.itest.ptm.ptm_web_service.GetTopicsByProfileIdAndActorIdResponse;
import it.lutech.c2sense.ptm.endpoint.PtmEndpoint;





@RunWith( SpringRunner.class )
@SpringBootTest
public class PtmEndpoinTests
{

	@Autowired
	private PtmEndpoint ptmEndpoint;

	@Before
	public void setUp( )
	{
	}

	@Test
	public void getTopicsByProfileIdAndActorId( )
	{
		GetTopicsByProfileIdAndActorIdRequest request = new GetTopicsByProfileIdAndActorIdRequest( );
		request.setProfileId( "alert" );
		request.setActorId( "alert_receiver" );
		GetTopicsByProfileIdAndActorIdResponse response = ptmEndpoint.getTopicsByProfileIdAndActorIdRequest( request );

		Assert.assertNotNull( response );
	}

	@Test
	public void getTopicNameByProfileIdAndTransactionIdRequest( )
	{
		GetTopicNameByProfileIdAndTransactionIdRequest request = new GetTopicNameByProfileIdAndTransactionIdRequest( );
		request.setProfileId( "alert" );
		request.setTransactionId( "send_alert" );

		GetTopicNameByProfileIdAndTransactionIdResponse response = ptmEndpoint.getTopicNameByProfileIdAndTransactionIdRequest( request );

		Assert.assertNotNull( response );
	}

}

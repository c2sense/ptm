
package it.lutech.c2sense.ptm;



import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import eu.c2_sense.itest.ptm.ptm_web_service.Actor;
import eu.c2_sense.itest.ptm.ptm_web_service.Profile;
import eu.c2_sense.itest.ptm.ptm_web_service.TopicMap;
import eu.c2_sense.itest.ptm.ptm_web_service.Transaction;
import it.lutech.c2sense.ptm.service.ActorService;
import it.lutech.c2sense.ptm.service.ProfileService;
import it.lutech.c2sense.ptm.service.TopicMapService;
import it.lutech.c2sense.ptm.service.TransactionService;





@RunWith( SpringRunner.class )
@SpringBootTest
public class PtmApplicationTests
{

	@Autowired
	private ActorService					actorService;
	@Autowired
	private ProfileService					profileService;
	@Autowired
	private TopicMapService					topicMapService;
	@Autowired
	private TransactionService				transactionService;

	it.lutech.c2sense.ptm.model.TopicMap	testTopicMap	= null;

	@Before
	public void setUp( )
	{
		testTopicMap = new it.lutech.c2sense.ptm.model.TopicMap( "alert", "Send Alert", "alert_receiver", "rule-11835", null );
	}

	@Test
	public void findActorTest( )
	{
		Actor actor = actorService.findActor( "alert_receiver" );
		Assert.assertNotNull( actor );
	}

	@Test
	public void listActorTest( )
	{
		List < Actor > actors = actorService.listActor( );
		Assert.assertNotNull( actors );
		Assert.assertFalse( actors.isEmpty( ) );
	}

	@Test
	public void findProfileTest( )
	{
		Profile profile = profileService.findProfile( "alert" );
		Assert.assertNotNull( profile );
	}

	@Test
	public void listProfileTest( )
	{
		List < Profile > profiles = profileService.listProfile( );
		Assert.assertNotNull( profiles );
		Assert.assertFalse( profiles.isEmpty( ) );
	}

	@Test
	public void findTransactionTest( )
	{
		Transaction transaction = transactionService.findTransaction( "allocate_resource" );
		Assert.assertNotNull( transaction );
	}

	@Test
	public void listTransactionTest( )
	{
		List < Transaction > transactions = transactionService.listTransaction( );
		Assert.assertNotNull( transactions );
		Assert.assertFalse( transactions.isEmpty( ) );
	}

	@Test
	public void findTopicMapByProfileIdAndActorIdTest( )
	{
		List < TopicMap > topicMap = topicMapService.findTopicMapByProfileIdAndActorId( "alert", "alert_receiver" );
		Assert.assertNotNull( topicMap );
		Assert.assertFalse( topicMap.isEmpty( ) );
		Assert.assertTrue( testTopicMap.equals( topicMap.get( 0 ) ) );
	}

	@Test
	public void findTopicMapByProfileIdAndTransactionIdTest( )
	{
		List < TopicMap > topicMap = topicMapService.findTopicMapByProfileIdAndTransactionId( "alert", "send_alert" );
		Assert.assertNotNull( topicMap );
		Assert.assertFalse( topicMap.isEmpty( ) );
		Assert.assertTrue( testTopicMap.equals( topicMap.get( 0 ) ) );
	}
}
